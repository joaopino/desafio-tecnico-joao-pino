# Desafio Técnico RedLight

##Teconologias usadas:
* Spring Boot com JPA
* Postgres

##Como Correr?
1. O primeiro passo é criar uma base de dados com postgres no localhost, porto 5432 com o nome e password: "desafiodb"
2. Correr o script de criação de tabelas: "assets/tabelas.sql"
3. Com o terminal correr o executável ./mvnw spring-boot:run
4. Abrir o localhost no port 8080.

##Escolhas tomadas e algumas notas

* O projeto foi todo desenvolvido no branch main, foi apenas feito porque tinha um backup no computador e estava a trabalhar sozinho
* O stack foi escolhido pois tive uma unidade curricular com spring boot e achei que seria interessante explorar a criação de uma API e uma webapp mais robusta com postgres e spring boot.
* Foi a primeira vez que usei HTML e CSS para algo além de apenas mostrar resultados de backend, portanto tive algumas dificuldades em fazer os popups. Porém fico feliz com o resultado. (Menos os position absolute)
* Acho que perdi algum tempo em fazer a responsividade das páginas um pouco desnecessariamente


#Nota: Ainda não foi totalmente implementado.

* Como foi a minha primeira vez a fazer uma webapp maior tive dificuldades com a parte do backend, gostava de ao fim do dia de segunda-feira ter terminado essa parte e voltar a adicionar a este repositório 
