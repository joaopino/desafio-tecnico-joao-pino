CREATE TABLE applicants (
	id		 BIGSERIAL,
	name	 VARCHAR(512) NOT NULL,
	phone_number BIGINT NOT NULL,
	avatar	 BOOL,
	PRIMARY KEY(id)
);

CREATE TABLE application (
	id		 BIGSERIAL,
	roles_id	 BIGINT NOT NULL,
	status_id	 BIGINT NOT NULL,
	applicants_id BIGINT NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE status (
	id	 BIGSERIAL,
	name VARCHAR(512) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE roles (
	id	 BIGSERIAL,
	name VARCHAR(512) NOT NULL,
	PRIMARY KEY(id)
);

ALTER TABLE application ADD CONSTRAINT application_fk1 FOREIGN KEY (roles_id) REFERENCES roles(id);
ALTER TABLE application ADD CONSTRAINT application_fk2 FOREIGN KEY (status_id) REFERENCES status(id);
ALTER TABLE application ADD CONSTRAINT application_fk3 FOREIGN KEY (applicants_id) REFERENCES applicants(id);

