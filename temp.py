import psycopg2

# Database connection details
host = "localhost"
port = "5432"
database = "desafiodb"
user = "postgres"
password = "postgres"

try:
    # Connect to the PostgreSQL database
    connection = psycopg2.connect(host=host, port=port, database=database, user=user, password=password)
    
    # Create a cursor object to execute SQL queries
    cursor = connection.cursor()
    
    # Create the "one" table (e.g., "roles")
    create_roles_table = """
        CREATE TABLE roles (
            id SERIAL PRIMARY KEY,
            name TEXT NOT NULL
        )
    """
    cursor.execute(create_roles_table)
    
    # Create the "many" table (e.g., "applicants")
    create_applicants_table = """
        CREATE TABLE applicants (
            id SERIAL PRIMARY KEY,
            name TEXT NOT NULL,
            role_id INTEGER REFERENCES roles(id)
        )
    """
    cursor.execute(create_applicants_table)
    
    # Commit the changes to the database
    connection.commit()
    
    print("Tables and many-to-one relationship created successfully.")
    
except (psycopg2.Error) as error:
    print("Error while connecting to PostgreSQL:", error)
    
finally:
    # Close the cursor and connection
    if cursor:
        cursor.close()
    if connection:
        connection.close()
