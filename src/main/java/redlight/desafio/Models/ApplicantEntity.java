package redlight.desafio.Models;

import jakarta.persistence.*;
import lombok.Data;


@Entity
@Table(name="applicants")
public class ApplicantEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private String name;

    @Column
    private long phone_number;

    //This one as an boolean is temporary
    @Column
    private boolean avatar;

    public ApplicantEntity(long id, String name, long phone_number, boolean avatar) {
        this.id = id;
        this.name = name;
        this.phone_number = phone_number;
        this.avatar = avatar;
    }
    public ApplicantEntity() {
    }
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public long getPhone_number() {
        return phone_number;
    }
    public void setPhone_number(long phone_number) {
        this.phone_number = phone_number;
    }
    public boolean isAvatar() {
        return avatar;
    }
    public void setAvatar(boolean avatar) {
        this.avatar = avatar;
    }


}
