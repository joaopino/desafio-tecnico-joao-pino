package redlight.desafio.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;

import jakarta.persistence.MappedSuperclass;



@Controller
public class PageContoller {
    
    @GetMapping("/")
    public String landingpage(){
        return "landingpage";
    }

    @GetMapping("/applicants")
    public String goToApplicants(){
        return "applicants";
    }

    
}
