package redlight.desafio.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;
import redlight.desafio.Models.ApplicantEntity;
import redlight.desafio.repository.ApplicantRepository;


@RestController
@RequestMapping("/applicant")
public class ApplicantController {
    
    private final ApplicantRepository applicantRepository;
    
    public ApplicantController(ApplicantRepository applicantRepository){
        this.applicantRepository = applicantRepository;
    }

    @GetMapping
    public ResponseEntity<List<ApplicantEntity>> getAlltheUsers(){
        return ResponseEntity.ok(this.applicantRepository.findAll());
    }


}
