package redlight.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import redlight.desafio.Models.ApplicantEntity;

@Repository
public interface ApplicantRepository extends JpaRepository<ApplicantEntity,Long>{
    
}
